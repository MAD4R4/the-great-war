﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    int _LevelIndex;

    [SerializeField]
    float _ShadowsTimeRatio = 6f;

    [SerializeField]
    GameObject _Shadows;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ShadowsLoop());
    }

    IEnumerator ShadowsLoop()
    {

        yield return new WaitForSeconds(_ShadowsTimeRatio);

        _Shadows.SetActive(true);
        StartCoroutine(ShadowsLoop());
    }


    public void StartTheGame()
    {
        SceneManager.LoadScene(_LevelIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }







}

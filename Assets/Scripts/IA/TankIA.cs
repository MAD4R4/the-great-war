﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
using System;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(StatusTank))]
public class TankIA : MonoBehaviour
{
    [SerializeField] [Range(.3f, 3)] private float turnSpeed;
    [SerializeField] private float Speed;
    [SerializeField] private float reloadTime;
    [SerializeField] private float bulletSpeed;
    [Space]
    [SerializeField] private float minDistToShoot;
    [SerializeField] private bool isPlayerAlly;
    [Space]
    //[SerializeField] private LayerMask enemysLayer;
    //[SerializeField] private GameObject TurrentGO;
    [SerializeField] private Transform DirectionGo;
    [SerializeField] private GameObject TargetToShoot;
    public Transform bulletSpawnPos;
    [SerializeField] private Bullet BulletPrefab;
    [SerializeField] private SpriteAdjustPerDegree adjustPerDegree;

    public Repair LinkedAllyTank;

    private NavMeshAgent agent;
    private Status status;
    //private Vector3 lastTargetPos;
    private bool isReloading = false;
    private Repair repair;

    private bool isWorking = true;
    private GameObject lastTarget;
    private bool beginShoot = false;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        status = GetComponent<Status>();
        repair = GetComponent<Repair>();
        agent.speed = Speed;

        repair.onBroke += Broke;
        repair.onCritic += Critic;
        repair.onMedium += Medium;

    }

    private void OnDestroy()
    {
        repair.onBroke -= Broke;
        repair.onCritic -= Critic;
        repair.onMedium -= Medium;

    }

    private void FixedUpdate()
    {
        if (isWorking)
        {
            if (TargetToShoot)
            {
                //AimOnTarget();
                if (!beginShoot)
                    Shoot();
            }
            else
            {
                FindTargetToShoot();
            }
        }
    }

    private void Broke()
    {
        isWorking = false;
        StopAllCoroutines();
        lastTarget = TargetToShoot;
        TargetToShoot = null;
        adjustPerDegree.target = null;
        beginShoot = false;
    }

    private void Critic()
    {
        isWorking = false;
        StopAllCoroutines();
        lastTarget = TargetToShoot;
        TargetToShoot = null;
        adjustPerDegree.target = null;
        beginShoot = false;

    }

    private void Medium()
    {
        isWorking = true;
        isReloading = false;
        TargetToShoot = lastTarget;
        //Shoot();
    }

    public void MoveToPoint(Vector3 pos)
    {
        Debug.Log("Moving");
        agent.SetDestination(pos);
    }

    public bool IsPlayerAlly()
    {
        return isPlayerAlly;
    }

    public void FindTargetToShoot()
    {

        List<GameObject> targets;
        if (isPlayerAlly)
            targets = BattleManager.m_EnemiesTanks;
        else
            targets = BattleManager.m_AllyTanks;

        var tank2DPos = new Vector2(transform.position.x, transform.position.z);

        for (int i = 0; i < targets.Count; i++)
        {
            var target = targets[i];
            var tankTarget2DPos = new Vector2(target.transform.position.x, target.transform.position.z);


            if (Vector2.Distance(tank2DPos, tankTarget2DPos) < minDistToShoot)
            {
                TargetToShoot = target;
                adjustPerDegree.target = target.transform;

                Debug.Log("Find Target!!", Color.green);
                return;
            }

            //if (i == targets.Count - 1)
            //{
            //    Debug.Log("No enemies in range!!");
            //}
        }

        if (LinkedAllyTank)
        {
            if (LinkedAllyTank.repairState == Repair.RepairState.Medium ||
                LinkedAllyTank.repairState == Repair.RepairState.Great)
            {
                agent.Move(DirectionGo.transform.forward * (Speed / 10));
            }
            else
            {
                Debug.Log("Other tank is with problems!!!");
            }
        }
        else
        {
            agent.Move(DirectionGo.transform.forward * (Speed / 10));
        }


    }

    private Vector3 GetRotation()
    {
        //var direction = mainCamera.transform.position - transform.position;
        var direction = TargetToShoot.transform.position - transform.position;
        direction.y = 0;

        var quaternionRotation = Quaternion.LookRotation(direction);
        var rotation = quaternionRotation.eulerAngles;
        var baseRotatio = transform.rotation.eulerAngles;
        return baseRotatio + rotation;
    }

    private void Shoot()
    {
        if (isReloading)
        {
            //Debug.Log("Try to shoot,but is reloading!!");
            return;
        }
        if (!TargetToShoot.GetComponent<Status>().isAlive)
        {
            TargetToShoot = null;
            return;
        }


        beginShoot = true;
        isReloading = true;
        Debug.Log("Shooting!!", this.gameObject, Color.red);

        var bullet = Instantiate(BulletPrefab, bulletSpawnPos.position, Quaternion.identity);
        bullet.InitBullet(bulletSpeed, this.isPlayerAlly, GetRotation());


        WaitForSeconds(reloadTime, () =>
        {
            Debug.Log("Reloaded!", this.gameObject, Color.green);
            isReloading = false;
            Shoot();
        });
    }

    private void WaitForSeconds(float time, Action onComplete)
    {
        StartCoroutine(CoWaitForSeconds(time, () => onComplete()));
    }

    private IEnumerator CoWaitForSeconds(float time, Action onComplete)
    {
        int id = UnityEngine.Random.Range(0, 100);

        Debug.Log("Reload time => " + time + " - ID : " + id, Color.yellow);
        yield return new WaitForSeconds(time);
        Debug.Log("End Reload!" + " - ID : " + id, Color.blue);
        onComplete();
    }

    private void OnDrawGizmosSelected()
    {
        DebugExtension.DrawCircle(transform.position + new Vector3(0, 1, 0), Vector3.up, Color.blue, minDistToShoot);
    }
}

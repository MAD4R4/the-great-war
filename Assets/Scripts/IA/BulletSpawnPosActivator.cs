﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawnPosActivator : MonoBehaviour
{
    [SerializeField] private TankIA tankIA;

    private void OnEnable()
    {
        tankIA.bulletSpawnPos = this.gameObject.transform;
    }
}

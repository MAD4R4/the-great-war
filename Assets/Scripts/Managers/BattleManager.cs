﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public List<GameObject> EnemiesTanks = new List<GameObject>();
    public List<GameObject> AllyTanks = new List<GameObject>();
    
    public static List<GameObject> m_EnemiesTanks = new List<GameObject>();
    public static List<GameObject> m_AllyTanks = new List<GameObject>();

    public static BattleManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public static void AddAlly(GameObject go)
    {
        if(!m_AllyTanks.Contains(go))
        {
            m_AllyTanks.Add(go);
        }
    }

    public static void AddEnemy(GameObject go)
    {
        if (!m_EnemiesTanks.Contains(go))
        {
            m_EnemiesTanks.Add(go);
        }
    }

    public static void RemoveAlly(GameObject go)
    {
        if (m_AllyTanks.Contains(go))
        {
            m_AllyTanks.Remove(go);
        }
    }

    public static void RemoveEnemy(GameObject go)
    {
        if (m_EnemiesTanks.Contains(go))
        {
            m_EnemiesTanks.Remove(go);
        }
    }


}

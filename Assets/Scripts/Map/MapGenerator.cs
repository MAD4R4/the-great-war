﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] public int Xsize;
    [SerializeField] public int Zsize;

    [SerializeField] public List<GameObject> Floors = new List<GameObject>();
    [SerializeField] public List<GameObject> Props = new List<GameObject>();

    [SerializeField] private GameObject ParentGo;

    [Header("Debug Visualize")]
    [SerializeField] private Vector3 size;
    [SerializeField] private Vector3 offSet;


    private Vector3 floorSize;
    private NavMeshSurface navMeshSurface;

    private void Awake()
    {
        floorSize = (Floors[0].GetComponent<MeshRenderer>().bounds.size);
        navMeshSurface = GetComponent<NavMeshSurface>();
    }

    private void Start()
    {
        GenerateMap();
    }

    private void GenerateMap()
    {
        for (int i = 0; i < Xsize; i++)
        {
            for (int j = 0; j < Zsize; j++)
            {
                var posX = (i - (Xsize / 2) ) * floorSize.x;
                var posZ = (j - (Zsize / 2) ) * floorSize.z;
                var pos = new Vector3(posX, 0, posZ);

                GenerateFloorTitle(pos);
            }
        }

        navMeshSurface.BuildNavMesh();
    }

    private void GenerateFloorTitle(Vector3 pos)
    {
        var rnd = Random.Range(0, Floors.Count-1);
        var selectedFloor = Floors[rnd];

        var title = Instantiate(selectedFloor, pos, selectedFloor.transform.rotation, ParentGo.transform);

        var x = Random.Range(-1f, 1f);
        var z = Random.Range(-1f, 1f);
        var propPos = new Vector3(x, 1.7f, z);

        GenerateProp(propPos, title);
    }

    private void GenerateProp(Vector3 pos,GameObject titleParent)
    {
        var rnd = Random.Range(0, Props.Count - 1);
        var selectedProp = Props[rnd];

        var obj =Instantiate(selectedProp, pos, selectedProp.transform.rotation);
        obj.transform.parent = titleParent.transform;
        obj.transform.localPosition = pos;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 finalSize = new Vector3(size.x * Xsize, size.y, size.z * Zsize);
        Gizmos.DrawWireCube(transform.position+ offSet, finalSize);
    }

}

﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepairUI : MonoBehaviour
{
    [SerializeField] private Canvas m_canvas;
    //[SerializeField] private Image Hp_Image;

    //[SerializeField] private Color GreatColor;
    //[SerializeField] private Color MediumColor;
    //[SerializeField] private Color BrokeColor;
    //[SerializeField] private Color CriticColor;

    [SerializeField] private SpriteRenderer repairIcon;

    [SerializeField] private Repair repair;

    public static Transform MainCamera;
    private float lastValue;

    private void Awake()
    {
        //repair.onBroke += () => { Hp_Image.color = BrokeColor; };
        //repair.onMedium += () => { Hp_Image.color = MediumColor; };
        //repair.onCritic += () => { Hp_Image.color = CriticColor; };
        //repair.onGreat += () => { Hp_Image.color = GreatColor; };

        if(tag != "Enemy")
        {
            repair.onBroke += () => 
            {
                repairIcon.enabled = true;
            };

            repair.onMedium += () =>
            {
                repairIcon.enabled = false;
            };

            repair.onGreat += () =>
            {
                repairIcon.enabled = false;
            };
        }



        if (MainCamera == null)
        {
            MainCamera = FindObjectOfType<Camera>().transform;
        }

    }

    private void OnDestroy()
    {
        repair.onBroke -= null;
        repair.onMedium -= null;
        repair.onCritic -= null;
        repair.onGreat -= null;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateUI();

        //FaceCamera(m_canvas.transform);
    }

    private void FaceCamera(Transform target)
    {
        var dir = target.transform.position - MainCamera.position;
        dir.y = 0;

        var targetRotation = Quaternion.LookRotation(dir);
        target.rotation = targetRotation;
    }

    private void UpdateUI()
    {
        //var hp = ((float)repair.status.hp / (float)repair.status.maxHP);

        //if (hp != lastValue)
        //{
        //    lastValue = hp;
        //    Hp_Image.DOFillAmount(hp, .3f);
        //}

        FaceCamera(m_canvas.transform);
    }



}

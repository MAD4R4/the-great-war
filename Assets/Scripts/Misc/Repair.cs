﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Status))]
public class Repair : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private float repairRange;
    [SerializeField] private float timeToRepair;
    [SerializeField] private int repairAmount;

    [SerializeField] private float burnRate;
    [SerializeField] private int burnDamage;
    
    [SerializeField] private float criticBurnRate;
    [SerializeField] private int criticBurnDamage;


    [HideInInspector] public StatusTank status;
    private static Transform playerT;

    public RepairState repairState;
    public enum RepairState
    {
        Great,
        Medium,
        Broke,
        Critic
    }

    public delegate void onRepairStateChange();
    public onRepairStateChange onBroke;
    public onRepairStateChange onCritic;
    public onRepairStateChange onMedium;
    public onRepairStateChange onGreat;

    private bool isVisible = true;

    private void Awake()
    {
        status = GetComponent<StatusTank>();
        if (playerT == null)
            playerT = FindObjectOfType<PlayerMovement>().transform;
    }

    private void FixedUpdate()
    {
        if(isVisible)
        {
            var dist = Vector3.Distance(this.transform.position, playerT.position);

            if ( dist < repairRange && (repairState != RepairState.Great) )
            {
                if(Input.GetButtonDown("Jump"))
                {
                    RepairTank();
                    PlayerMovement.playerAnim.SetTrigger("Repair");
                }
            }
        }

    }

    public void ChangeState(RepairState st)
    {
        repairState = st;

        switch (st)
        {
            case RepairState.Great:
                onGreat?.Invoke();
                break;
            case RepairState.Medium:
                onMedium?.Invoke();
                break;
            case RepairState.Broke:
                BeginBurn();
                onBroke?.Invoke();
                break;
            case RepairState.Critic:
                BeginBurn();
                onCritic?.Invoke();
                break;
        }

    }

    public void RepairTank()
    {
        status.IncreaseHP(repairAmount);
    }

    public void BeginBurn()
    {
        StopAllCoroutines();
        StartCoroutine(Burn());
    }

    private IEnumerator Burn()
    {
        if(repairState == RepairState.Broke)
        {
            yield return new WaitForSeconds(burnRate);
            status.DecreaseHP(burnDamage);
            StartCoroutine(Burn());
        }
        else if(repairState == RepairState.Critic)
        {
            yield return new WaitForSeconds(criticBurnRate);
            status.DecreaseHP(criticBurnDamage);
            StartCoroutine(Burn());
        }

    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnDrawGizmosSelected()
    {
        DebugExtension.DrawCircle(transform.position + new Vector3(0, 1, 0), Vector3.up, Color.red, repairRange);
    }

    


}

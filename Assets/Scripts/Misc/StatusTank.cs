﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusTank : Status
{
    [SerializeField] private Repair repair;
    [SerializeField] private GameObject FxFire;
    
    private TankIA tankIA;

    private void Awake()
    {
        tankIA = GetComponent<TankIA>();
    }

    private void OnEnable()
    {
        if(tankIA.IsPlayerAlly())
        {
            BattleManager.AddAlly(this.gameObject);
        }
        else
        {
            BattleManager.AddEnemy(this.gameObject);
        }
    }



    private void CheckHealthState()
    {
        var percentage = ((float)hp / (float)maxHP)*100;
        //Debug.Log("Percentage => " + percentage);
        if ( percentage <= 20f)
        {
            repair.ChangeState(Repair.RepairState.Critic);
        }
        else if( percentage > 20f && percentage <= 40f)
        {
            repair.ChangeState(Repair.RepairState.Broke);
        }
        else if (percentage > 40f && percentage <= 80f)
        {
            repair.ChangeState(Repair.RepairState.Medium);
        }
        else if(percentage > 80)
        {
            repair.ChangeState(Repair.RepairState.Great);
        }
    }

    public new void DecreaseHP(int amount)
    {
        if (!isAlive) return;

        if (hp - amount <= 0)
        {
            Die();
        }
        else
        {
            hp -= amount;
            CheckHealthState();
        }
    }

    public new void IncreaseHP(int amount)
    {
        if (hp + amount >= maxHP)
        {
            hp = maxHP;
        }
        else
        {
            hp += amount;
        }
        CheckHealthState();

    }

    public new void Die()
    {
        isAlive = false;

        var fire = Instantiate(FxFire, new Vector3(0, 1, 0),FxFire.transform.rotation);
        fire.transform.parent = this.transform;
        fire.transform.localPosition = new Vector3(0, 1, 0);

        if (tankIA.IsPlayerAlly())
        {
            BattleManager.RemoveAlly(this.gameObject);
        }
        else
        {
            BattleManager.RemoveEnemy(this.gameObject);
        }


    }





}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Custom Editor
#if UNITY_EDITOR

using UnityEditor;

[CustomEditor(typeof(SpritePerDegrees))]
public class SpritePerDegressCE : Editor
{
    [SerializeField] private SerializedProperty m_DegressToChange;
    [SerializeField] private SerializedProperty m_SpritesToDegreeRange;
    //[SerializeField] private SerializedProperty m_XMirror;
    //[SerializeField] private SerializedProperty m_YMirror;
    //[SerializeField] private SerializedProperty m_spriteRenderer;
    [SerializeField] private SerializedProperty rotationY;
    [SerializeField] private SerializedProperty target;

    private void OnEnable()
    {
        m_DegressToChange = serializedObject.FindProperty("degreesToChange");
        m_SpritesToDegreeRange = serializedObject.FindProperty("SpritesToDegreeRange");
        //m_spriteRenderer = serializedObject.FindProperty("spriteRenderer");
        rotationY = serializedObject.FindProperty("rotationY");
        target = serializedObject.FindProperty("target");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        serializedObject.Update();

        if (360 % m_DegressToChange.floatValue != 0)
        {
            EditorGUI.LabelField(new Rect(3, 3, 300, 20),
                "Esse angulo não fecha em 360 graus!!");
            EditorGUILayout.Space(20, true);
        }

        //EditorGUILayout.PropertyField(m_spriteRenderer);
        EditorGUILayout.PropertyField(m_DegressToChange);
        EditorGUILayout.PropertyField(rotationY); //Para debugar o angulo do objeto com relação a camera
        EditorGUILayout.PropertyField(target);
        EditorGUILayout.Space(20, true);

        int sprites = (int)(360 / m_DegressToChange.floatValue);
        var ThisList = m_SpritesToDegreeRange;

        if (sprites != ThisList.arraySize)
        {
            while (sprites > ThisList.arraySize)
            {
                ThisList.InsertArrayElementAtIndex(ThisList.arraySize);
            }
            while (sprites < ThisList.arraySize)
            {
                ThisList.DeleteArrayElementAtIndex(ThisList.arraySize - 1);
            }
        }

        for (int i = 0; i < sprites; i++)
        {
            EditorGUILayout.Space(10, true);

            var target = ThisList.GetArrayElementAtIndex(i);
            var range = target.FindPropertyRelative("degreesRange");

            EditorGUI.BeginDisabledGroup(true);

            //var sprite = target.FindPropertyRelative("sprite");
            //EditorGUILayout.PropertyField(sprite);

            range.stringValue = "Range : " + (i * m_DegressToChange.floatValue).ToString() + " - " + ((i + 1) * m_DegressToChange.floatValue);
            EditorGUILayout.PropertyField(range);

            var minRange = target.FindPropertyRelative("minRange");
            var maxRange = target.FindPropertyRelative("maxRange");

            minRange.floatValue = (i * m_DegressToChange.floatValue);
            maxRange.floatValue = ((i + 1) * m_DegressToChange.floatValue);

            EditorGUI.EndDisabledGroup();

            SerializedProperty m_spriteRendererTarget = target.FindPropertyRelative("spriteRendererTarget");
            EditorGUILayout.PropertyField(m_spriteRendererTarget);

            //var xMirror = target.FindPropertyRelative("XMirror");
            //var yMirror = target.FindPropertyRelative("YMirror");

            //ACTIVE TO DEBUG

            //var min = target.FindPropertyRelative("minRange");
            //var max = target.FindPropertyRelative("maxRange");
            //EditorGUILayout.PropertyField(min);
            //EditorGUILayout.PropertyField(max);

            //


            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.PropertyField(xMirror);
            //EditorGUILayout.PropertyField(yMirror);
            //EditorGUILayout.EndHorizontal();
        }


        serializedObject.ApplyModifiedProperties();
    }

}

#endif
#endregion

public class SpritePerDegrees : MonoBehaviour
{
    /// <summary>
    /// A cada quantos de rotação ele muda de sprite
    /// </summary>
    [SerializeField] private float degreesToChange = 60;
    [SerializeField] private List<SpritePerDegreeStruct> SpritesToDegreeRange;
    //[SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private float rotationY;
    public Transform target;
    private int lastIndex = 0;
    //public static Camera mainCamera;

    private void Awake()
    {
        //if (mainCamera == null)
        //{
        //    mainCamera = FindObjectOfType<Camera>();
        //}
    }

    void FixedUpdate()
    {
        ChangeSprite();
    }

    private void ChangeSprite()
    {

        var rotation = transform.rotation.eulerAngles + GetRotation();
        var clampedRotation = Clamp0360(rotation.y);
        rotationY = clampedRotation;

        if (clampedRotation > SpritesToDegreeRange[lastIndex].minRange &&
            clampedRotation <= SpritesToDegreeRange[lastIndex].maxRange)
        {
            return;
        }
        else
        {
            for (int i = 0; i < SpritesToDegreeRange.Count; i++)
            {
                if (i == lastIndex) continue;

                var target = SpritesToDegreeRange[i];

                if (clampedRotation > target.minRange &&
                    clampedRotation <= target.maxRange)
                {
                    SpritesToDegreeRange[lastIndex].spriteRendererTarget.enabled = false;
                    target.spriteRendererTarget.enabled = true;
                    lastIndex = i;
                    //spriteRenderer.sprite = target.sprite;
                    //spriteRenderer.flipX = target.XMirror;
                    //spriteRenderer.flipY = target.YMirror;
                }
            }
        }
    }

    private Vector3 GetRotation()
    {
        //var direction = mainCamera.transform.position - transform.position;
        var direction = target.position - transform.position;
        direction.y = 0;

        var quaternionRotation = Quaternion.LookRotation(direction);
        var rotation = quaternionRotation.eulerAngles;

        return rotation;
    }

    public float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }


}

[System.Serializable]
public struct SpritePerDegreeStruct
{
    public string degreesRange;
    //public Sprite sprite;
    //public bool XMirror;
    //public bool YMirror;
    public SpriteRenderer spriteRendererTarget;

    public float minRange;
    public float maxRange;
}

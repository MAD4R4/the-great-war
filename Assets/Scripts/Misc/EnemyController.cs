﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Status))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{

    [SerializeField] private float reloadTime;
    [SerializeField] private float speed;

    [SerializeField] private Bullet BulletPrefab;
    [SerializeField] private Transform bulletSpawnPos;
    [SerializeField] private float bulletSpeed;

    [SerializeField] private float minDistToFindEnemy;
    [SerializeField] private Transform DirectionGo;

    [SerializeField] private Transform target;

    [SerializeField] private Animator anim;


    private NavMeshAgent agent;
    private bool isReloading = false;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        //anim = GetComponent<Animator>();

        agent.speed = this.speed;
    }

    private void Start()
    {
        Invoke("PlaceOnMesh", .3f);
    }

    private void PlaceOnMesh()
    {
        agent.Warp(transform.position);
    }

    public void Move()
    {
        agent.Move(DirectionGo.transform.forward * (speed / 10));

    }

    private void FixedUpdate()
    {
        anim.SetBool("isWalking", (target == null));

        if (!target)
        {
            Move();
        }
        else
        {
            Shoot();
        }


    }


    private Vector3 GetRotation()
    {
        //var direction = mainCamera.transform.position - transform.position;
        var direction = target.transform.position - transform.position;
        direction.y = 0;

        var quaternionRotation = Quaternion.LookRotation(direction);
        var rotation = quaternionRotation.eulerAngles;
        var baseRotatio = transform.rotation.eulerAngles;
        return baseRotatio + rotation;
    }

    private void Shoot()
    {
        if (isReloading || target == null)
        {
            return;
        }
        if (!target.GetComponent<Status>().isAlive)
        {
            target = null;
            return;
        }

        anim.SetTrigger("Attack");

        var bullet = Instantiate(BulletPrefab, bulletSpawnPos.position, Quaternion.identity);
        bullet.InitBullet(bulletSpeed, false, GetRotation());

        isReloading = true;

        WaitForSeconds(reloadTime, () =>
        {
            isReloading = false;
            Shoot();
        });
    }

    private void WaitForSeconds(float time, Action onComplete)
    {
        StartCoroutine(CoWaitForSeconds(time, () => onComplete()));
    }

    private IEnumerator CoWaitForSeconds(float time, Action onComplete)
    {
        yield return new WaitForSeconds(time);
        onComplete();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Ally")
        {
            target = other.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == target.gameObject)
        {
            target = null;
        }
    }

    private void OnDrawGizmosSelected()
    {
        DebugExtension.DrawCircle(transform.position + new Vector3(0, 1, 0), Vector3.up, Color.blue, minDistToFindEnemy);
    }
}

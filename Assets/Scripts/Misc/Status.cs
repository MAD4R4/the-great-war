﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Status : MonoBehaviour
{
    [SerializeField] public int maxHP;
    [SerializeField] public int hp;

    public delegate void onDie();
    public static onDie onDieEvent;
    public bool isAlive = true;

    private void Awake()
    {
        hp = maxHP;
    }

    public void DecreaseHP(int amount)
    {
        if(hp-amount <= 0)
        {
            Die();
        }
        else
        {
            hp -= amount;
        }
    }

    public void IncreaseHP(int amount)
    {
        if (hp + amount >= maxHP)
        {
            hp = maxHP;
        }
        else
        {
            hp += amount;
        }
    }

    protected void Die()
    {
        onDieEvent?.Invoke();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    [SerializeField] private int minDamage;
    [SerializeField] private int maxDamage;
    [SerializeField] private Bullet bullet;

    private void Awake()
    {
        bullet.onCollidedEvent += (target) => DamageTarget(target);
    }

    public void DamageTarget(GameObject target)
    {
        try
        {
            var stTank = target.GetComponent<StatusTank>();

            var damage = Random.Range(minDamage, maxDamage + 1);
            stTank.DecreaseHP(damage);
        }
        catch
        {
            DamageTarget(target.GetComponent<Status>());
        }
    }

    private void DamageTarget(Status target)
    {
        var damage = Random.Range(minDamage, maxDamage + 1);
        target.DecreaseHP(damage);
    }



}

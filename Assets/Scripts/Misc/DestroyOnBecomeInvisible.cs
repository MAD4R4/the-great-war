﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnBecomeInvisible : MonoBehaviour
{
    [SerializeField] private Transform parentGO;

    private void OnBecameInvisible()
    {
        Destroy(parentGO.gameObject);
    }
}

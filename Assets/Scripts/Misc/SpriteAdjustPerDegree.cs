﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct spritesAdjusts
{
    public List<GameObject> objectToActive;
    //public bool xMirror;
    //public bool YMirror;
    public float minAngle;
    public float maxAngle;
}




public class SpriteAdjustPerDegree : MonoBehaviour
{
    public Transform target;
    [SerializeField] private float ActualRotation;
    [SerializeField] private List<spritesAdjusts> adjusts;

    private int lastIndex = 0;
    private List<GameObject> allGO = new List<GameObject>();

    private void Awake()
    {
        for (int i = 0; i < adjusts.Count; i++)
        {
            var target = adjusts[i];

            for (int j = 0; j < target.objectToActive.Count; j++)
            {
                var objTarget = target.objectToActive[j];
                objTarget.SetActive(false);

                if (!allGO.Contains(objTarget))
                {
                    allGO.Add(objTarget);
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeSprite(360 - Clamp0360(transform.rotation.eulerAngles.y));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target != null)
            ChangeSprite(GetRotationToTarget());
    }

    private float GetRotationToTarget()
    {
        var rotation = transform.rotation.eulerAngles + GetRotation();
        var clampedRotation = 360 - Clamp0360(rotation.y);
        ActualRotation = clampedRotation;
        return clampedRotation;
    }


    private void ChangeSprite(float clampedRotation)
    {


        if (clampedRotation > adjusts[lastIndex].minAngle &&
            clampedRotation <= adjusts[lastIndex].maxAngle)
        {
            return;
        }
        else
        {
            for (int i = 0; i < adjusts.Count; i++)
            {
                if (i == lastIndex) continue;

                var target = adjusts[i];

                if (clampedRotation > target.minAngle &&
                    clampedRotation <= target.maxAngle)
                {

                    //Desativar objetos anteriores
                    for (int k = 0; k < adjusts[lastIndex].objectToActive.Count; k++)
                    {
                        var objTarget = adjusts[lastIndex].objectToActive[k];
                        objTarget.SetActive(false);
                    }

                    //Ativando novos objetos
                    for (int k = 0; k < adjusts[i].objectToActive.Count; k++)
                    {
                        var objTarget = adjusts[i].objectToActive[k];
                        objTarget.SetActive(true);
                    }

                    lastIndex = i;
                }
            }
        }
    }

    private Vector3 GetRotation()
    {
        //var direction = mainCamera.transform.position - transform.position;
        var direction = target.position - transform.position;
        direction.y = 0;

        var quaternionRotation = Quaternion.LookRotation(direction);
        var rotation = quaternionRotation.eulerAngles;

        return rotation;
    }

    public float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug : UnityEngine.Debug
{
    /// <summary>
    /// Debug a message in console
    /// </summary>
    /// <param name="message"></param>
    public static void Log(string message)
    {
        UnityEngine.Debug.Log(message);
    }

    /// <summary>
    /// Debug a message in console with color 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="color"></param>
    public static void Log(string message, Color color)
    {
        UnityEngine.Debug.Log("<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">" + message + "</color>");
    }

    /// <summary>
    /// Debug a message in console with color and highlight a object who send the message
    /// </summary>
    /// <param name="message"></param>
    /// <param name="color"></param>
    public static void Log(string message,GameObject Context, Color color)
    {
        UnityEngine.Debug.Log("<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">" + message + "</color>", Context);
    }

    /// <summary>
    /// Debug a message in console and highlight a object who send the message
    /// </summary>
    /// <param name="message"></param>
    /// <param name="color"></param>
    public static void Log(string message, GameObject Context)
    {
        UnityEngine.Debug.Log(message,Context);
    }
}

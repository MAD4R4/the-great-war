﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    private Rigidbody rigid;
    [SerializeField] private float speed;
    [SerializeField] private bool isPlayerAlly;
    [SerializeField] private UnityEvent onCollide;

    public delegate void onCollided(GameObject Collided);
    public onCollided onCollidedEvent;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    public void InitBullet(float speed, bool isPlayerAlly)
    {
        this.speed = speed;
        rigid.velocity = transform.forward * this.speed;
        this.isPlayerAlly = isPlayerAlly;
    }

    public void InitBullet(float speed, bool isPlayerAlly, Vector3 rotation)
    {
        this.speed = speed;
        transform.eulerAngles = rotation;
        rigid.velocity = transform.forward * this.speed;
        this.isPlayerAlly = isPlayerAlly;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isPlayerAlly)
        {
            if (other.tag == "Enemy")
            {
                onCollide.Invoke();
                onCollidedEvent?.Invoke(other.gameObject);
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (other.tag == "Player" || other.tag == "Ally")
            {
                onCollide.Invoke();
                onCollidedEvent?.Invoke(other.gameObject);
                Destroy(this.gameObject);
            }
        }

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] private float ZSpeedCompenserMultiplier;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Animator anim;

    public static SpriteRenderer spriteRenderer;
    public static Animator playerAnim;

    private void Awake()
    {
        agent.speed = Speed;
        playerAnim = anim;
        spriteRenderer = anim.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 input = new Vector3(horizontal, 0, vertical);

        anim.SetBool("isWalking", (input.magnitude > 0));
        spriteRenderer.flipX = (horizontal < 0);

        if (agent.enabled)
        {
            if ((horizontal != 0 && vertical != 0) || (horizontal != 0 && vertical == 0))
                agent.Move(input.normalized * Speed * Time.deltaTime);
            else if ((horizontal == 0 && vertical != 0))
                agent.Move(input.normalized * (Speed * ZSpeedCompenserMultiplier) * Time.deltaTime);
        }
    }


}

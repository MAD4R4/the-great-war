﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableShadows : MonoBehaviour
{
    public void DisableShadowObj()
    {
        gameObject.SetActive(false);
    }
}
